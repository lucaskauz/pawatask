import Vue from 'vue'
import { mapGetters } from 'vuex'

import { Getters, Task } from '@/store/index'

interface ModalParams {
  params: {
    taskId: string
  }
}

export interface Data {
  taskFetched: boolean;
  taskId: null | string;
}

export default Vue.extend({
  data (): Data {
    return {
      taskId: null,
      taskFetched: false
    }
  },
  computed: {
    ...mapGetters({
      getTaskById: Getters.GET_TASK_BY_ID
    }),
    currentTask (): Task | null {
      return this.taskId ? this.getTaskById(this.taskId) : null
    }
  },
  methods: {
    beforeOpen ({ params }: ModalParams) {
      if (!params || !params.taskId) {
        this.taskFetched = true
        return
      }

      this.taskId = params.taskId
      this.taskFetched = true
    },
    closed () {
      this.taskId = null
      this.taskFetched = false
    }
  }
})
